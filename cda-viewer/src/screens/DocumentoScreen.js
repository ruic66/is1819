import React, { Component } from 'react';
import '../App.css';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import { Toolbar } from '@material-ui/core';
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";
import { isAuthenticated, logout, getToken} from '../auth'
import axios from 'axios';
import LoadingComponent from '../components/LoadingComponent';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Divider from '@material-ui/core/Divider';

class DocumentoScreen extends Component {

    constructor (props) {
        super(props)
        this.state = {
            data: null,
            loading: true,
            documentID: this.props.match.params.id
        }
        this.handleLogout = this.handleLogout.bind(this)
    }

    componentWillMount() {
        if (!isAuthenticated()) this.props.history.push("/");
    }

    componentDidMount() {
        axios.get('/documents/documentData/'+this.props.match.params.id, { headers: { Authorization: 'Bearer ' + getToken() } })
             .then(response => {
                    this.setState({
                        data: response.data,
                        loading: false
                    })
                })
             .catch(err => {})
    }

    handleLogout() {
        logout()
        console.log(getToken())
        this.props.history.push("/");
    }


    buildStructure(o, level){
        level++;
        for(var a in o){
            if (typeof o[a] == 'object'){
                if(a!=="$"){
                    if(a==='name'){
                        var jointName = ''
                        Object.entries(o[a]).forEach(name => {
                            if(name[0]!=='$' && typeof(name[1]) !== 'object'){
                                jointName += `${name[1]} `
                            }
                        })
                        o[a] = jointName;
                    }
                    if(a==='addr'){
                        var jointAddr = ''
                        Object.entries(o[a]).forEach(addr => {
                            if(addr[0]!=='$' && typeof(addr[1]) !== 'object'){
                                jointAddr += `${addr[1]}, `
                            }
                        })
                        o[a] = jointAddr;
                    }
                    if(a==='structuredBody'){
                        var texts = [];
                        Object.entries(o[a].component).forEach(component => {
                            console.log(component[1])
                            var text = component[1].section.text
                            texts.push(text)
                        })
                        for(var i = 0; i < o[a].component.length; i++){
                            Object.entries(o[a].component[i].section).forEach(sec => {
                                o[a].component[i][`${sec[0]}`] = sec[1];
                            })
                            delete o[a].component[i].section;
                        }
                    }
                }
                //else s = 
                this.buildStructure(o[a],level)
            }
            else{
                //console.log(a)
            }//end if
        }//end for
        if(level === 1){
            console.log(o.ClinicalDocument.component)
            o.ClinicalDocument.component = o.ClinicalDocument.component.structuredBody.component
        }
    }//end functionc


    buildDocument(o, level, parent){
        let s = null;
        var aux = [];
        level++;
        for(var a in o){
            if (typeof o[a] == 'object'){
                if(a!=="$"){
                    s = (
                            <div style={{paddingTop:"20px"}}>
                                <ExpansionPanel>
                                    <ExpansionPanelSummary
                                        expandIcon={<ExpandMoreIcon/>}
                                        aria-controls={a}
                                        id={a}
                                    >
                                        <div style={{fontStyle:"nasaFont"}}>
                                            {(!isNaN(+a) && parent === null) 
                                                ? o[a].title
                                                : a}
                                        </div>
                                    </ExpansionPanelSummary>
                                        <Divider light/>
                                <ExpansionPanelDetails>
                                    {this.buildDocument(o[a],level,a)}
                                </ExpansionPanelDetails>
                                </ExpansionPanel>
                            </div>
                    );
                }
                else s = this.buildDocument(o[a],level,o)
                aux.push(s);                
            }
            else{
                if(a!=='title'){
                    s = (
                        <div style={{fontStyle:"nasaFont" ,paddingTop:"20px"}}>
                            <b>{a}: </b> {o[a]}
                        </div>
                    )
                    aux.push(s);
                }
            }//end if
        }//end for
        let p = (
            <div style={{width:"100%"}}>
                {aux.map(item => {
                    return(
                        <div style={{width:"100%"}}>
                            {item}
                        </div>
                    )
                })}
            </div>
        )
        return p;
    }//end functionc
    
  render() {
    let tree = [];

    if(!this.state.loading){
        console.log(this.state.data)
        this.buildStructure(this.state.data,0)
        var rawDate = this.state.data.ClinicalDocument.effectiveTime.$.value;
        var parsedDate = `${rawDate.substring(0,4)}/${rawDate.substring(4,6)}/${rawDate.substring(6,8)}`
        var html = this.buildDocument(this.state.data.ClinicalDocument.component,0,null)
        return (
            <div>
                <AppBar className="nav nasaFont" color="default" position="sticky">
                    <Toolbar color="inherit">
                    <h1 className="brand">HL7 CDA</h1>
                    <div className="menuButtons">
                    <NavLink className="link" activeClassName="active" exact to="/uploadCDA"><Button variant="outlined" >Upload CDA</Button></NavLink>
                    <NavLink className="link" activeClassName="active" to="/listarCDA"><Button variant="outlined" >View CDA</Button></NavLink>
                    <Button variant="outlined" className="button" onClick={this.handleLogout}>Logout</Button>
                </div>
    
                    </Toolbar>
                </AppBar>
                <div style={{margin: '4em'}} >
                <h1>
                    {this.state.data.ClinicalDocument.title}
                </h1>
                <span>
                    <b>Language:</b> {this.state.data.ClinicalDocument.languageCode.$.code}
                    <span class='space' style={{marginLeft: '1em'}} />
                    <b>Author: </b> {this.state.data.ClinicalDocument.author.assignedAuthor.assignedPerson.name}
                    <span class='space' style={{marginLeft: '1em'}} />
                    <b>Version: </b> {this.state.data.ClinicalDocument.versionNumber.$.value}
                </span>
                <br/>
                <span>
                    <b>Custodian:</b> {this.state.data.ClinicalDocument.custodian.assignedCustodian.representedCustodianOrganization.name}
                </span>
                <br/>
                <span>
                    <b>Patient:</b> {this.state.data.ClinicalDocument.recordTarget.patientRole.patient.name}
                </span>
                <br/>
                <span>
                    <b>Issued at:</b> {parsedDate}
                </span>
                {html}
                </div>
            </div>
        );
    }
    else{
        return(
            <div className="centerDiv">
            <LoadingComponent />
            </div>
        )
    }
    
  }
  
}

export default DocumentoScreen;