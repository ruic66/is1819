import React, { Component } from 'react';
import '../App.css';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import { Toolbar } from '@material-ui/core';
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";
import { isAuthenticated, logout, getToken} from '../auth'
import axios from 'axios';
import DocumentList from '../components/DocumentList';
import LoadingComponent from '../components/LoadingComponent';

class ListarScreen extends Component {

    constructor (props) {
        super(props)
        this.state = {
            documents: [],
            loading: true
        }
        this.handleLogout = this.handleLogout.bind(this)
    }

    componentWillMount() {
        if (!isAuthenticated()) this.props.history.push("/");
    }

    componentDidMount() {
        axios.get('/documents', { headers: { Authorization: 'Bearer ' + getToken() } })
             .then(response => {
                 console.log(response)
                 this.setState({
                     documents: response.data,
                     loading: false
                    })
                })
             .catch(err => {})
    }

    handleLogout() {
        logout()
        console.log(getToken())
        this.props.history.push("/");
    }

  render() {
    if(!this.state.loading){
        return (
            <div>
                <AppBar className="nav nasaFont" color="default" position="sticky">
                    <Toolbar color="inherit">
                    <h1 className="brand">HL7 CDA</h1>
                    <div className="menuButtons">
                    <NavLink className="link" activeClassName="active" exact to="/uploadCDA"><Button variant="outlined" >Upload CDA</Button></NavLink>
                    <NavLink className="link" activeClassName="active" to="/listarCDA"><Button variant="outlined" >View CDA</Button></NavLink>
                    <Button variant="outlined" className="button" onClick={this.handleLogout}>Logout</Button>
                </div>
    
                    </Toolbar>
                </AppBar>
                <h1 style={{margin: '1em'}}>Lista de documentos CDA</h1>
                <div className='displayContainer'>
                    <DocumentList documents={this.state.documents}/>
                </div>
            </div>
        );
    }
    else{
        return(
            <div className="centerDiv">
                <LoadingComponent />
            </div>
        )
    }
    
  }
  
}

export default ListarScreen;