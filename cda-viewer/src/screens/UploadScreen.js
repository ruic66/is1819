import React, { Component } from 'react';
import '../App.css';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import Card from '@material-ui/core/Card';
import { Toolbar } from '@material-ui/core';
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";
import { isAuthenticated, logout, getToken} from '../auth'
import axios from 'axios';
import { FilePicker } from 'react-file-picker'
var jwt = require('jsonwebtoken')

class UploadScreen extends Component {

    constructor (props) {
        super(props)
        this.state = {file: {}, errMsg: "", done: false}
        this.handleLogout = this.handleLogout.bind(this)
        this.handleUpload = this.handleUpload.bind(this)
    }

    componentWillMount() {
        if (!isAuthenticated()) this.props.history.push("/");
    }


    handleLogout() {
        logout()
        console.log(getToken())
        this.props.history.push("/");
    }

    handleUpload() {
        var decodedToken = jwt.decode(getToken(), {complete: true});
        let formData = new FormData(); 
        formData.append('ficheiro', this.state.file);
        formData.append('userid', decodedToken.payload.user.userid)
        axios.post('/documents', formData, { headers: {'content-type': 'multipart/form-data', Authorization: 'Bearer ' + getToken() } })
             .then(response => {alert('Upload realizado com sucesso. ')})
             .catch(err => {})
    }

  render() {
    if (this.state.done === true) {
        var snack = (<Snackbar>Feito.</Snackbar>)
    } else {
        var snack = null
    }
    return (
        <div>
            {snack}
            <AppBar className="nav" color="default" position="sticky">
                <Toolbar color="inherit">
                <h1 className="brand">HL7 CDA</h1>
                <div className="menuButtons">
                    <NavLink className="link" activeClassName="active" exact to="/uploadCDA"><Button variant="outlined" >Upload CDA</Button></NavLink>
                    <NavLink className="link" activeClassName="active" to="/listarCDA"><Button variant="outlined" >View CDA</Button></NavLink>
                    <Button variant="outlined" className="button" onClick={this.handleLogout}>Logout</Button>
                </div>

                </Toolbar>
            </AppBar>
            <div className='uploadContainer'>
            <Card className='uploadCard'>
            <p style={{fontSize: 20, color: 'red'}}>{this.state.errMsg}</p>
            <FilePicker
    extensions={['xml']}
    onChange={FileObject => {this.setState({file: FileObject, errMsg: ""})}}
    onError={errMsg => {this.setState({file: {}, errMsg: errMsg})}}
    >
    <Button variant="outlined" className="button">Choose File</Button>
    </FilePicker>
            <p>Chosen file: {this.state.file.name}</p>
            <Button variant="outlined" className="button" onClick={this.handleUpload}>Upload file</Button>
            </Card>
            </div>
        </div>
    );
  }
  
}

export default UploadScreen;