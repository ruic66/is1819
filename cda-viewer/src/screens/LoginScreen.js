import React, { Component } from 'react';
import '../App.css';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";
import { login, isAuthenticated } from '../auth'
var axios = require('axios')

class LoginScreen extends Component {

    constructor (props) {
        super(props)
        this.state = {
            email: "", 
            username: "",
            password: "", 
            error: "", 
            loginStyle: {borderBottom: "2px solid lightskyblue"}, 
            registerStyle: {},
            chosen: 0
        }
        this.handleEmailChange = this.handleEmailChange.bind(this)
        this.handlePasswordChange = this.handlePasswordChange.bind(this)
        this.handleUsernameChange = this.handleUsernameChange.bind(this)
        this.handleLogin = this.handleLogin.bind(this)
        this.handleRegister = this.handleRegister.bind(this)
        this.handleChosen = this.handleChosen.bind(this)
    }

    componentWillMount() {
        if (isAuthenticated()) this.props.history.push("/listarCDA");
    }

    handleChosen(a) {
        if (a === 0) this.setState({
            loginStyle: {borderBottom: "2px solid lightskyblue"}, 
            registerStyle: {}, 
            chosen: 0})
        else {
            this.setState({
                loginStyle: {}, 
                registerStyle: {borderBottom: "2px solid lightskyblue"}, 
                chosen: 1})
        }
    }

    handleLogin() {
        axios.post('/auth/login', {email: this.state.email, password: this.state.password})
             .then(response => {
                 if (response.data.token) {
                    login(response.data.token)
                    this.setState({error: ''})
                    this.props.history.push("/listarCDA");
                 } else {
                     this.setState({error: 'Credenciais inválidas.'})
                 }
                })
             .catch(err => {})
    }

    handleRegister() {
        axios.post('/auth/registo', {email: this.state.email, password: this.state.password, username: this.state.username, type: "All"})
             .then(response => {alert('Registo realizado com sucesso.')})
             .catch(err => {})
    }

    handlePasswordChange(event) {
        this.setState({password: event.target.value})
    }

    handleUsernameChange(event) {
        this.setState({username: event.target.value})
    }

    handleEmailChange(event) {
        this.setState({email: event.target.value})
    }

  render() {
    let cardContent;

    if (this.state.chosen === 0) {
        cardContent = (
            <form>
            <h3>Iniciar Sessão</h3>
            <p style={{color:'red', fontSize: '20px'}}>{this.state.error}</p>
            <TextField
                 id="outlined-name"
                label="Email"
                margin="normal"
                variant="outlined"
                value={this.state.email}
                onChange={this.handleEmailChange}
            />
            <br/>
            <TextField
                 id="outlined-name"
                label="Password"
                margin="normal"
                variant="outlined"
                type="password"
                value={this.state.password}
                onChange={this.handlePasswordChange}
            />
            <br/>
           <Button onClick={this.handleLogin} variant="outlined" color='primary' className='loginButton'>
    Entrar
  </Button>
  </form>
        )
    } else {
        cardContent = (
            <form>
            <h3>Registar</h3>
            <p style={{color:'red', fontSize: '20px'}}>{this.state.error}</p>
            <TextField
                 id="outlined-name"
                label="Username"
                margin="normal"
                variant="outlined"
                type="text"
                value={this.state.username}
                onChange={this.handleUsernameChange}
            />
            <br/>
            <TextField
                 id="outlined-name"
                label="Email"
                margin="normal"
                variant="outlined"
                value={this.state.email}
                onChange={this.handleEmailChange}
            />
            <br/>
            <TextField
                 id="outlined-name"
                label="Password"
                margin="normal"
                variant="outlined"
                type="password"
                value={this.state.password}
                onChange={this.handlePasswordChange}
            />
            <br/>
           <Button onClick={this.handleRegister} variant="outlined" color='primary' className='loginButton'>
    Registar
  </Button>
  </form>
        )
    }

    return (
        <div className='overviewContainer'>
            <Grid container spacing={24} >
            <Grid item xs={6} className='centerContent rightSeparator'>
              <h2>HL7 CDA</h2>
              <h3>Viewer and Uploader</h3>
            </Grid>
            <Grid item xs={6} className="centerContent">
            <Card className='loginCard'>
                <CardContent className='loginCardContent'>
                        <div className="loginMenu">
                        <Button onClick={() => this.handleChosen(0)} style={this.state.loginStyle} className="menuButton">Iniciar Sessão</Button>
                        <Button onClick={() => this.handleChosen(1)} style={this.state.registerStyle} className="menuButton">Registar</Button>
                        </div>
                        {cardContent}

                </CardContent>
            </Card>


            </Grid>
          </Grid>
        </div>
    );
  }
  
}

export default LoginScreen;