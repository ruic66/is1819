import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";
import LoginScreen from "./screens/LoginScreen"
import ListarScreen from './screens/ListarScreen';
import UploadScreen from './screens/UploadScreen';
import DocumentoScreen from './screens/DocumentoScreen';

class App extends Component {
  render() {
    return (
        <Router>
          <Route exact path="/" component={LoginScreen} />
          <Route exact path="/listarCDA" component={ListarScreen} />
          <Route exact path="/uploadCDA" component={UploadScreen} />
          <Route exact path="/documento/:id" component={DocumentoScreen} />
        </Router>
    );
  }
}

export default App;