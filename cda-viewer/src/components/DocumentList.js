import React from 'react';
import LoadingComponent from './LoadingComponent';
import DocumentCard from './DocumentCard';

class DocumentList extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            loading: true
        }
    }

    componentDidMount(){
        this.setState({
            documents: this.props.documents,
            loading: false
        }, () => {
            console.log(this.props.documents);
        })
    }

    render(){
        if(!this.state.loading){
            return(
                <div>
                    {this.state.documents.map(document => {
                        return(
                            <DocumentCard document={document} />
                        )
                    })}
                </div>
            )
        }
        else{
            return(
                <LoadingComponent/>
            )
        }
    }
}

export default DocumentList;