import React from 'react';
import {PushSpinner} from 'react-spinners-kit';

class LoadingComponent extends React.Component{
    render() {
        return (
            <PushSpinner
                size={30}
                color="#686769"
                loading={true}
            />
        );
    }
}

export default LoadingComponent;