import React from 'react';
import {Link} from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import PersonIcon from '@material-ui/icons/Person';
import CardIcon from '@material-ui/icons/CreditCard'
import LoadingComponent from './LoadingComponent';
import FolderIcon from '@material-ui/icons/Folder';
import AttachmentIcon from '@material-ui/icons/Attachment';


class DocumentCard extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            loading: true,
            document : this.props.document,
        }
    }

    componentDidMount(){
        this.setState({
            loading: false
        })
    }

    render(){
        if(!this.state.loading){
            return(
                <div className='documentList'>
                    <List>
                        <ListItem>
                            <ListItemIcon>
                                <PersonIcon />
                            </ListItemIcon>
                            <ListItemText>
                                <b>Nome do paciente: </b>{this.state.document.patientName}
                            </ListItemText>
                        </ListItem>
                        <Divider/>
                        <ListItem>
                            <ListItemIcon>
                                <CardIcon />
                            </ListItemIcon>
                            <ListItemText>
                                <b>Identificador do paciente: </b>{this.state.document.patientID}
                            </ListItemText>
                        </ListItem>
                        <Divider />
                        <ListItem>
                            <ListItemIcon>
                                <FolderIcon />
                            </ListItemIcon>
                            <ListItemText>
                                <b>Tipo do documento: </b>{this.state.document.type}
                            </ListItemText>
                        </ListItem>
                        <Divider />
                        <Link 
                            to={`/documento/${this.state.document._id}`}
                            style={{textDecoration:'none'}}
                        >
                            <ListItem>
                                <ListItemIcon>
                                    <AttachmentIcon />
                                </ListItemIcon>
                                <ListItemText>
                                    <b>Ver documento</b>
                                </ListItemText>
                            </ListItem>
                        </Link>
                    </List>
                </div>
            )
        }
        else{
            return(
                <LoadingComponent />
            )
        }
    }

}

export default DocumentCard;
