var DocumentModel = require('../models/documentModel')


/* listar documentos */
module.exports.listar = () => {
    return DocumentModel.find()
                        .lean()
                        .exec()
}

/* mostar documentos por _id */
module.exports.listarID = id => {
    return DocumentModel.findOne({_id : id})
                        .exec()
}

/* listar documentos de um tipo especifico*/
module.exports.listarTipo = type => {
    return DocumentModel.find({type : type})
                        .exec()
}

/* mostar documentos de um determinado paciente */
module.exports.consultarPaciente = id => {
    return DocumentModel.find({patientID : id})
                        .exec()
}

/* listar tipos de documentos existentes */
module.exports.tipos = () => {
    return DocumentModel.find()
                        .distinct('type')
                        .exec()
}

/* listar pacientes existentes */
module.exports.pacientes = () => {
    return DocumentModel.find()
                        .distinct('patientID')
                        .exec()
}

/* inserir documento */
module.exports.inserir = document => {
    return DocumentModel.create(document)
}

/* apagar documento */
module.exports.apagar = documentID => {
    return DocumentModel.findByIdAndRemove(documentID)
                        .exec()
}