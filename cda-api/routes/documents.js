var express = require('express');
var formidable = require('formidable');
var mv = require('mv');
var fs = require('fs');
var bcrypt = require('bcrypt')
var jwt = require('jsonwebtoken')
var passport = require('passport')
var xml2json = require('xml2js');
var DocumentController = require('../controllers/documentController')
var router = express.Router();

/* Autentica */
router.get('/*', passport.authenticate('jwt', {session: false}), (req, res, next) => {next()})
router.post('/*', passport.authenticate('jwt', {session: false}), (req, res, next) => {next()})

/* GET - Listar documentos. */
router.get('/documents', function(req, res, next) {
    DocumentController.listar()
                      .then(dados => {res.jsonp(dados)})
                      .catch(erro => {res.status(500).jsonp(erro)})

});

/* GET - Listar documentos de um determinado tipo. */
router.get('/documents/byType/:type', function(req, res, next) {
    var type = req.params.type
    DocumentController.listarTipo(type)
                      .then(dados => {res.jsonp(dados)})
                      .catch(erro => {res.status(500).jsonp(erro)})

});

/* GET - Listar documentos de um determinado paciente. */
router.get('/documents/byPatient/:patientID', function(req, res, next) {
    var patientID = req.params.patientID
    DocumentController.consultarPaciente(patientID)
                      .then(dados => {res.jsonp(dados)})
                      .catch(erro => {res.status(500).jsonp(erro)})

});

/* GET - Mostrar documento específico. */
router.get('/documents/byID/:id', function(req, res, next) {
    var id = req.params.id
    DocumentController.listar()
                      .then(dados => {
                        res.jsonp(dados.filter(doc => doc._id == 1))
                      })
                      .catch(erro => {res.status(500).jsonp(erro)})

});

/* GET - Listar tipos de documentos existentes. */
router.get('/documents/types', function(req, res, next) {
    DocumentController.tipos()
                      .then(dados => {res.jsonp(dados)})
                      .catch(erro => {res.status(500).jsonp(erro)})

});

/* GET - Listar pacientes existentes */
router.get('/documents/patients', function(req, res, next) {
    DocumentController.pacientes()
                      .then(dados => {res.jsonp(dados)})
                      .catch(erro => {res.status(500).jsonp(erro)})

});

/* GET - Obter informação em json de um ficheiro guardado, por ID */
router.get('/documents/documentData/:id', function(req,res,next){
    console.log(req.params.id)
    DocumentController.listarID(req.params.id)
                        .then(dados => {
                            var parser = new xml2json.Parser({explicitArray: false})
                            let xmlFile = fs.readFileSync(dados.documentPath,'utf8');
                            parser.parseString(xmlFile,(err,data)=>{
                                res.jsonp(data)
                            })
                        })
                        .catch(erro => {console.log(erro);res.status(500).jsonp(erro)})
})

/* POST - Criar novo documento */
router.post('/documents/', function(req, res) {
    var form = new formidable.IncomingForm()
    form.parse(req, (err, fields, files)=>{
        if (files.ficheiro === undefined) {
            res.status(500).json('erro')
            return 
        }
        var fenviado = files.ficheiro.path
        var fnovo = './documents/' + files.ficheiro.name
        mv(fenviado, fnovo, (erro)=>{
            if(!erro){
                    fs.readFile( fnovo, function(err, data) {
                    var parser = new xml2json.Parser({explicitArray: false})
                    parser.parseString(data , function(err,json) {
                        //console.dir(JSON.stringify(json.ClinicalDocument.typeId[0].$.root))
                        var metadata = {
                            type : json.ClinicalDocument.typeId.$.root,
                            uploaderID: fields.userid,
                            patientID : json.ClinicalDocument.recordTarget.patientRole.id[0].$.root,
                            patientName : json.ClinicalDocument.recordTarget.patientRole.patient.name.given[0] + " " + json.ClinicalDocument.recordTarget.patientRole.patient.name.family,
                            documentPath : fnovo
                        }
                         DocumentController.inserir(metadata)
                          .then(dados => {res.jsonp(dados)})
                          .catch(erro => {res.status(500).jsonp(erro)})
                    });
                });
            }
            else {
                res.status(500).end();
                console.log(erro);
            }
        }) 
    })
});

/* DELETE - Eliminar documento */
router.delete('/documents/:id', function(req, res) {
    DocumentController.apagar(req.params.id)
                      .then(() => {res.end()})
                      .catch(erro => console.log(erro))
});

/* POST - Criar novo documento */
router.post('/documents/outro', function(req, res) {
    DocumentController.inserir(req.body)
                      .then(dados => {res.jsonp(dados)})
                      .catch(erro => {res.status(500).jsonp(erro)})
});

module.exports = router;