var express = require('express');
var passport = require('passport');
var jwt = require('jsonwebtoken')
var router = express.Router();


/* POST processa registo. */
router.post('/registo', (req, res, next) => {
    passport.authenticate('registo', (err, user, info) => { 
            if(err) return next(err)
            else res.jsonp(user)
    })(req, res, next);
  });

/* GET processa logout. */
router.get('/logout', function(req, res) {
    req.logout()
    res.clearCookie('token')
    res.end()
  });


/* POST processa login. */
router.post('/login', (req, res, next) => {
    passport.authenticate('login', (err, user, info) => { 
            if(!user){
              return res.redirect('/')
            } 
            else if (err) { return next(err); }
            else {
            req.login(user, { session : false }, (error) => {
                if (error) return next(error)
                var myUser = {userid: user.userid, 
                  username: user.username, 
                  email: user.email, 
                  type: user.tipo}
                var token = jwt.sign({ user : myUser }, 'is2018');
                res.cookie('token', token);
                return res.json({token});
            });
          }
    })(req, res, next);
  });

module.exports = router;
