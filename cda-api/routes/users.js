var express = require('express');
var UserController = require('../controllers/userController')
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  UserController.listAll()
                .then(dados => {res.jsonp(dados)})
                .catch(erro => {res.status(500).jsonp(erro)})
});

module.exports = router;
