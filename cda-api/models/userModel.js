var mongoose = require('mongoose')
var bcrypt = require('bcrypt')
const AutoIncrement = require('mongoose-sequence')(mongoose);

/* User Schema */
var UserSchema = new mongoose.Schema(
    {
        userid          : {type: Number},
        username        : {type: String, required: true},
        email           : {type: String, required: true},
        password        : {type: String, required: true}
    })

UserSchema.plugin(AutoIncrement, {inc_field:'userid'});

/* Antes de Save executa callback */
UserSchema.pre('save', async function (next){
    var hash = await bcrypt.hash(this.password, 10)
    this.password = hash
    next()
})


/* Metodo para verificar password */
UserSchema.methods.isValidPassword = async function (password) {
    var compare = await bcrypt.compare(password, this.password)
    return compare
}

/* User Model */
var UserModel = mongoose.model('UserSchema', UserSchema ,'users')

/* Export User Model */
module.exports = UserModel