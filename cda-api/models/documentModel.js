var mongoose = require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);

/* Alias */
var Schema = mongoose.Schema

var DocumentSchema = new Schema ({
    docid           : {type: Number},
    type            : {type: String, required: true},
    uploaderID      : {type: Number, required: true},
    patientID       : {type: String, required: true},
    patientName    : {type: String, required: true},
    documentPath    : {type: String, required: true}
})

DocumentSchema.plugin(AutoIncrement, {inc_field: 'docid'});

module.exports = mongoose.model('Document', DocumentSchema, 'documents')